package messaging

type Message interface {
	GetSubject() string
	GetPlain() string
	GetHtml() string
}

type MessageImpl struct {
	Subject string
	Plain   string
	Html    string
}

func (m MessageImpl) GetSubject() string {
	return m.Subject
}

func (m MessageImpl) GetPlain() string {
	return m.Plain
}

func (m MessageImpl) GetHtml() string {
	return m.Html
}
