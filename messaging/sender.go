package messaging

import "log"

type Sender interface {
	Send(to, from Email, message Message) error
}

type LogSender struct {
}

func (ls LogSender) Send(to, from Email, message Message) error {
	log.Println("To:", to.GetName(), "-", to.GetAddress())
	log.Println("From:", from.GetName(), "-", from.GetAddress())
	log.Println("--- Message ---")
	log.Println(message.GetSubject())
	log.Println(message.GetPlain())
	log.Println("---------------")

	return nil
}
