package messaging

type Email interface {
	GetName() string
	GetAddress() string
}

type EmailImpl struct {
	Name    string
	Address string
}

func (e EmailImpl) GetName() string {
	return e.Name
}

func (e EmailImpl) GetAddress() string {
	return e.Address
}
