package messaging

import "io"

type Messenger interface {
	RenderMessagePlain(w io.Writer, key string, data map[string]interface{}) error
	RenderMessageHtml(w io.Writer, key string, data map[string]interface{}) error
}
