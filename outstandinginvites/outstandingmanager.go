package outstandinginvites

import (
	"gitlab.com/high-creek-software/minotaur/pendinguser"
	"gitlab.com/high-creek-software/minotaur/savingsender"
	"log/slog"
)

type OutstandingManager struct {
	subject            string
	pendingUserManager pendinguser.PendingUserManager
	savingSender       *savingsender.SavingSender
}

func NewOutstandingManager(subject string, pendingUserManager pendinguser.PendingUserManager, sr *savingsender.SavingSender) *OutstandingManager {
	return &OutstandingManager{
		subject:            subject,
		pendingUserManager: pendingUserManager,
		savingSender:       sr,
	}
}

func (om *OutstandingManager) ListOutstandingInvites() ([]OutstandingInvite, error) {
	pending, err := om.pendingUserManager.List()
	if err != nil {
		return nil, err
	}

	var invites []OutstandingInvite
	for _, p := range pending {
		if sm, err := om.savingSender.FindByTarget(p.Email, om.subject); err == nil && len(sm) > 0 {
			invites = append(invites, OutstandingInvite{
				User:    p,
				Message: sm[0],
			})
		} else {
			slog.Error("error loading saved message", "error", err, "for", p.Email)
		}
	}

	return invites, nil
}

type OutstandingInvite struct {
	User    pendinguser.PendingUser   `json:"user"`
	Message savingsender.SavedMessage `json:"message"`
}
