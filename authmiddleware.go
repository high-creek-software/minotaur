package minotaur

import (
	"gitlab.com/high-creek-software/minotaur/usertoken"
	"log"
	"net/http"
	"time"
)

const (
	defaultTokenKey       = "minotaur-token"
	defaultTokenHeaderKey = "X-Auth-Token"

	defaultSessionTTL = 30 * 24 * time.Hour
)

type ErrorResponse int

const (
	Redirect ErrorResponse = iota
	ResponseCode
)

type AuthMiddleware[R comparable, U UserInfo[R]] struct {
	tokenHeaderKey string
	tokenKey       string
	loginRedirect  string
	sessionAdapter SessionAdapter
	authManager    AuthManager[R, U]
	sessionTTL     time.Duration
}

func NewAuthMiddleware[R comparable, U UserInfo[R]](sessionAdapter SessionAdapter, authManager AuthManager[R, U]) *AuthMiddleware[R, U] {
	am := &AuthMiddleware[R, U]{
		tokenHeaderKey: defaultTokenHeaderKey,
		tokenKey:       defaultTokenKey,
		loginRedirect:  defaultLoginRoute,
		sessionAdapter: sessionAdapter,
		authManager:    authManager,
		sessionTTL:     defaultSessionTTL,
	}

	return am
}

func (am *AuthMiddleware[R, U]) findOpaqueToken(w http.ResponseWriter, r *http.Request) (string, error) {
	opaqueToken := r.Header.Get(am.tokenHeaderKey)
	if opaqueToken != "" {
		return opaqueToken, nil
	}

	tokenInter, tokenErr := am.sessionAdapter.GetValue(w, r, am.tokenKey)
	if tokenErr != nil {
		return "", tokenErr
	}
	return tokenInter, nil
}

func (am *AuthMiddleware[R, U]) LoggedInRedirectMiddleware(to string) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			opaqueToken, _ := am.findOpaqueToken(w, r)
			if opaqueToken != "" {
				user, userToken, userErr := am.authManager.LoadUser(opaqueToken)
				if userErr == nil {
					usertoken.SetToken(userToken.Token, r)
					SetUserInfo[R](user, r)
					http.Redirect(w, r, to, http.StatusFound)
					return
				}
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (am *AuthMiddleware[R, U]) CheckAuthenticated() func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			opaqueToken, _ := am.findOpaqueToken(w, r)
			if opaqueToken != "" {
				user, userToken, userErr := am.authManager.LoadUser(opaqueToken)
				if userErr == nil {
					usertoken.SetToken(userToken.Token, r)
					SetUserInfo[R](user, r)
				}
			}
			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}

func (am *AuthMiddleware[R, U]) SecurityMiddleware(errorResponse ErrorResponse, validRoles ...R) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			opaqueToken, _ := am.findOpaqueToken(w, r)

			if opaqueToken == "" {
				if errorResponse == Redirect {
					am.setErrorFlash(w, r, "authentication required for: "+r.URL.Path)
					am.errorRedirect(w, r)
					return
				} else {
					http.Error(w, "authentication required for: "+r.URL.Path, http.StatusUnauthorized)
					return
				}
			}

			user, userToken, userErr := am.authManager.LoadUser(opaqueToken)
			if userErr != nil {
				log.Println("error loading user from middleware", userErr)
				if errorResponse == Redirect {
					am.setErrorFlash(w, r, "authentication required for: "+r.URL.Path)
					am.errorRedirect(w, r)
					return
				} else {
					http.Error(w, "authentication required for: "+r.URL.Path, http.StatusUnauthorized)
					return
				}
			}

			// Check if the user has one of the roles in the valid roles list
			userHasRole := false
			if len(validRoles) == 0 {
				userHasRole = true
			} else {
				userRoles := user.GetRoles()
			ValidRoles:
				for _, validRole := range validRoles {
					for _, userRole := range userRoles {
						if validRole == userRole {
							userHasRole = true
							break ValidRoles
						}
					}
				}
			}

			if !userHasRole {
				if errorResponse == Redirect {
					am.setErrorFlash(w, r, "role required for: "+r.URL.Path)
					am.errorRedirect(w, r)
					return
				} else {
					http.Error(w, "authentication required for: "+r.URL.Path, http.StatusUnauthorized)
					return
				}
			}

			usertoken.SetToken(userToken.Token, r)
			SetUserInfo[R](user, r)

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (ah *AuthMiddleware[R, U]) DoLogout(w http.ResponseWriter, r *http.Request) {
	token, ok := usertoken.TokenFromContext(r)
	if !ok {
		log.Println("token not found in context")
		ah.errorRedirect(w, r)
		return
	}

	delErr := ah.authManager.DeleteToken(token)
	if delErr != nil {
		log.Println("error deleting token", delErr)
		ah.errorRedirect(w, r)
		return
	}

	err := ah.sessionAdapter.ResetSession(w, r)
	if err != nil {
		log.Println("error resetting session", err)
		ah.setErrorFlash(w, r, "error logging out")
		ah.errorRedirect(w, r)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func (ah *AuthMiddleware[R, U]) AuthenticateUser(w http.ResponseWriter, r *http.Request, userInfo U, fingerprint string) error {
	userToken, err := ah.authManager.AuthenticateUser(userInfo, fingerprint)
	if err != nil {
		return err
	}

	err = ah.SetTokenOnSession(w, r, userToken.Token)

	return err
}

func (ah *AuthMiddleware[R, U]) SetTokenOnSession(w http.ResponseWriter, r *http.Request, token string) error {
	err := ah.sessionAdapter.SetValue(w, r, ah.tokenKey, token, int(ah.sessionTTL.Seconds()))
	return err
}

func (ah *AuthMiddleware[R, U]) setErrorFlash(w http.ResponseWriter, r *http.Request, message string) error {
	return ah.sessionAdapter.SetErrorFlash(w, r, message)
}

func (ah *AuthMiddleware[R, U]) setSuccessFlash(w http.ResponseWriter, r *http.Request, message string) error {
	return ah.sessionAdapter.SetSuccessFlash(w, r, message)
}

func (ah *AuthMiddleware[R, U]) errorRedirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, ah.loginRedirect, http.StatusFound)
}

func (ah *AuthMiddleware[R, U]) SetTokenKey(tokenKey string) *AuthMiddleware[R, U] {
	ah.tokenKey = tokenKey

	return ah
}

func (ah *AuthMiddleware[R, U]) SetTokenHeaderKey(tokenHeaderKey string) *AuthMiddleware[R, U] {
	ah.tokenHeaderKey = tokenHeaderKey
	return ah
}

func (ah *AuthMiddleware[R, U]) SetSessionTTL(sessionTTL time.Duration) *AuthMiddleware[R, U] {
	ah.sessionTTL = sessionTTL

	return ah
}
