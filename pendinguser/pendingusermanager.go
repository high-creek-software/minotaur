package pendinguser

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"time"

	"github.com/rs/xid"
	"gitlab.com/high-creek-software/minotaur"
	"gitlab.com/high-creek-software/minotaur/messaging"
	"gitlab.com/high-creek-software/minotaur/passwords"
)

const (
	defaultRedeemTemplate = "userredeem"
	defaultSubject        = "New Account"
)

type PendingUserManager interface {
	AddPendingUser(name, email string) error
	DecodePendingUser(r io.Reader) error
	FindPendingUser(id, token string) (PendingUser, error)
	DeletePendingUser(id string) error
	List() ([]PendingUser, error)
}

type PendingUserManagerImpl struct {
	repo             PendingUserRepo
	messenger        messaging.Messenger
	sender           messaging.Sender
	passwordEncoder  passwords.PasswordEncoder
	fromEmailAddress string
	redeemTemplate   string
	subject          string
}

func (p *PendingUserManagerImpl) AddPendingUser(name, email string) error {
	token, err := minotaur.GenerateRandomURLSafeString(50)
	if err != nil {
		return err
	}
	hashedToken, err := p.passwordEncoder.Encode(token)
	if err != nil {
		return err
	}
	now := time.Now()
	pu := &PendingUser{ID: xid.New(), CreatedAt: now, UpdatedAt: now, Name: name, Email: email, Secret: hashedToken}
	err = p.repo.Store(pu)
	if err != nil {
		return err
	}
	data := map[string]any{"ID": pu.ID, "Token": token}
	var plain bytes.Buffer
	err = p.messenger.RenderMessagePlain(&plain, p.redeemTemplate, data)
	if err != nil {
		p.repo.Delete(pu.ID.String())
		return err
	}
	err = p.sender.Send(messaging.EmailImpl{Name: pu.Name, Address: pu.Email}, messaging.EmailImpl{Name: "Add User", Address: p.fromEmailAddress}, messaging.MessageImpl{Plain: string(plain.Bytes()), Subject: p.subject})
	if err != nil {
		p.repo.Delete(pu.ID.String())
		log.Println("error sending out user redeem email", err)
		return err
	}
	return nil
}

func (p *PendingUserManagerImpl) DecodePendingUser(r io.Reader) error {
	var pu PendingUser
	err := json.NewDecoder(r).Decode(&pu)
	if err != nil {
		return err
	}

	return p.AddPendingUser(pu.Name, pu.Email)
}

func (p *PendingUserManagerImpl) FindPendingUser(id, token string) (PendingUser, error) {
	pu, err := p.repo.Find(id)
	if err != nil {
		return PendingUser{}, err
	}
	err = p.passwordEncoder.Matches(token, pu.Secret)
	if err != nil {
		return PendingUser{}, err
	}

	return *pu, nil
}

func (p *PendingUserManagerImpl) DeletePendingUser(id string) error {
	return p.repo.Delete(id)
}

func (p *PendingUserManagerImpl) List() ([]PendingUser, error) {
	return p.repo.List()
}

func NewPendingUserManager(repo PendingUserRepo, messenger messaging.Messenger, sender messaging.Sender, passwordEncoder passwords.PasswordEncoder, fromEmailAddress string) *PendingUserManagerImpl {
	pm := &PendingUserManagerImpl{
		repo:             repo,
		messenger:        messenger,
		sender:           sender,
		passwordEncoder:  passwordEncoder,
		redeemTemplate:   defaultRedeemTemplate,
		fromEmailAddress: fromEmailAddress,
		subject:          defaultSubject,
	}

	return pm
}

func (p *PendingUserManagerImpl) SetSubject(subject string) *PendingUserManagerImpl {
	p.subject = subject

	return p
}

func (p *PendingUserManagerImpl) SetRedeemTemplate(redeem string) *PendingUserManagerImpl {
	p.redeemTemplate = redeem

	return p
}
