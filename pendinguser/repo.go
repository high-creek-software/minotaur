package pendinguser

type PendingUserRepo interface {
	Store(pu *PendingUser) error
	Find(id string) (*PendingUser, error)
	Delete(id string) error
	List() ([]PendingUser, error)
}
