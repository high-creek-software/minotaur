package pendinguser

import (
	"strings"

	"github.com/Masterminds/squirrel"
)

type builder struct {
	squirrel.StatementBuilderType
}

func (b builder) buildStore(pu *PendingUser) (string, []any, error) {
	return b.Insert(puName).
		Columns("id", "created_at", "name", "email", "secret").
		Values(pu.ID, pu.CreatedAt.Unix(), pu.Name, strings.ToLower(pu.Email), pu.Secret).
		ToSql()
}

func (b builder) buildFind(id string) (string, []any, error) {
	return b.Select("id", "created_at", "name", "email", "secret").
		From(puName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder) buildDelete(id string) (string, []any, error) {
	return b.Delete(puName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder) buildList() (string, []any, error) {
	return b.Select("id", "created_at", "name", "email").
		From(puName).
		ToSql()
}
