package pendinguser

import (
	"time"

	"github.com/rs/xid"
)

type PendingUser struct {
	ID        xid.ID `json:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
	Name      string `json:"name"`
	Email     string `json:"email"`
	Secret    string
}

func (PendingUser) TableName() string {
	return "pending_user"
}
