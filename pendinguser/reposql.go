package pendinguser

import (
	"database/sql"
	"github.com/Masterminds/squirrel"
	"log"
	"log/slog"
)

const (
	createPendingUser = `CREATE TABLE IF NOT EXISTS pending_user(
    id varchar(36) primary key,
    created_at datetime,
    "name" varchar(1024),
    email varchar(1024),
    secret text
);`

	puName = "pending_user"
)

type PendingUserRepoSql struct {
	builder
	db *sql.DB
}

func (p *PendingUserRepoSql) Store(pu *PendingUser) error {
	ins, args, err := p.buildStore(pu)
	if err != nil {
		return err
	}
	_, err = p.db.Exec(ins, args...)
	return err
}

func (p *PendingUserRepoSql) Find(id string) (*PendingUser, error) {
	sel, args, err := p.buildFind(id)
	if err != nil {
		return nil, err
	}
	row := p.db.QueryRow(sel, args...)

	var pu PendingUser

	err = row.Scan(&pu.ID, &pu.CreatedAt, &pu.Name, &pu.Email, &pu.Secret)
	if err != nil {
		return nil, err
	}

	return &pu, nil
}

func (p *PendingUserRepoSql) Delete(id string) error {
	del, args, err := p.buildDelete(id)
	if err != nil {
		return nil
	}
	_, err = p.db.Exec(del, args...)
	return err
}

func (p *PendingUserRepoSql) List() ([]PendingUser, error) {
	sel, _, err := p.buildList()
	rows, err := p.db.Query(sel)
	if err != nil {
		return nil, err
	}

	var pendingUsers []PendingUser
	for rows.Next() {
		var pu PendingUser
		if err := rows.Scan(&pu.ID, &pu.CreatedAt, &pu.Name, &pu.Email); err == nil {
			pendingUsers = append(pendingUsers, pu)
		} else {
			slog.Error("error scanning pending user", "error", err)
		}
	}

	return pendingUsers, nil
}

func NewPendingUserRepoSql(db *sql.DB, sbt squirrel.StatementBuilderType) PendingUserRepo {

	_, err := db.Exec(createPendingUser)
	if err != nil {
		log.Fatalln("error creating pending user", err)
	}

	return &PendingUserRepoSql{db: db, builder: builder{StatementBuilderType: sbt}}
}
