package pendinguser

import (
	"log"
	"strings"

	"gorm.io/gorm"
)

type PendingUserRepoImpl struct {
	db *gorm.DB
}

func (p *PendingUserRepoImpl) Store(pu *PendingUser) error {
	pu.Email = strings.ToLower(pu.Email)
	return p.db.Create(pu).Error
}

func (p *PendingUserRepoImpl) Find(id string) (*PendingUser, error) {
	var pendingUser PendingUser
	err := p.db.Find(&pendingUser, "id = ?", id).Error

	return &pendingUser, err
}

func (p *PendingUserRepoImpl) Delete(id string) error {
	return p.db.Where("id = ?", id).Delete(&PendingUser{}).Error
}

func (p *PendingUserRepoImpl) List() ([]PendingUser, error) {
	panic("not yet implemented")
}

func NewPendingUserRepoImpl(db *gorm.DB) PendingUserRepo {
	err := db.AutoMigrate(&PendingUser{})
	if err != nil {
		log.Fatalln(err)
	}

	return &PendingUserRepoImpl{db: db}
}
