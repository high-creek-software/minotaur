package magiclink

import "gorm.io/gorm"

type MagicLinkRepoGorm struct {
	db *gorm.DB
}

func NewMagicLinkRepoGorm(db *gorm.DB) *MagicLinkRepoGorm {
	mlr := &MagicLinkRepoGorm{db: db}
	db.AutoMigrate(&MagicLink{})
	return mlr
}

func (mr *MagicLinkRepoGorm) Store(magicLink MagicLink) error {
	return mr.db.Create(&magicLink).Error
}

func (mr *MagicLinkRepoGorm) Find(id string) (MagicLink, error) {
	var ml MagicLink
	err := mr.db.First(&ml, "id = ?", id).Error
	return ml, err
}

func (mr *MagicLinkRepoGorm) Delete(id string) error {
	return mr.db.Where("id = ?", id).Delete(&MagicLink{}).Error
}
