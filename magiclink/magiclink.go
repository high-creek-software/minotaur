package magiclink

import (
	"github.com/rs/xid"
	"time"
)

type MagicLink struct {
	ID        xid.ID `json:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Token     string     `json:"token"`
	UserID    string     `json:"userId"`
}

func (MagicLink) TableName() string {
	return "magic_link"
}
