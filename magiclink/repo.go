package magiclink

type MagicLinkRepo interface {
	Store(magicLink MagicLink) error
	Find(id string) (MagicLink, error)
	Delete(id string) error
}
