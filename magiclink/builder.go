package magiclink

import "github.com/Masterminds/squirrel"

const (
	mlName = "magic_link"
)

type builder struct {
	squirrel.StatementBuilderType
}

func (b builder) buildStore(ml MagicLink) (string, []any, error) {
	return b.Insert(mlName).
		Columns("id", "token", "user_id").
		Values(ml.ID, ml.Token, ml.UserID).
		ToSql()
}

func (b builder) buildFind(id string) (string, []any, error) {
	return b.Select("id", "token", "user_id").
		From(mlName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder) buildDelete(id string) (string, []any, error) {
	return b.Delete(mlName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}
