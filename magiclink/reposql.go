package magiclink

import (
	"database/sql"
	"github.com/Masterminds/squirrel"
	"log"
)

const (
	createMagicLink = `CREATE TABLE IF NOT EXISTS magic_link(
    id varchar(36) primary key,
    token varchar(256) not null,
    user_id varchar(36) not null
);`
)

type MagicLinkRepoSql struct {
	builder
	db *sql.DB
}

func (m *MagicLinkRepoSql) Store(magicLink MagicLink) error {
	ins, args, err := m.buildStore(magicLink)
	if err != nil {
		return err
	}
	_, err = m.db.Exec(ins, args...)
	return err
}

func (m *MagicLinkRepoSql) Find(id string) (MagicLink, error) {
	sel, args, err := m.buildFind(id)
	if err != nil {
		return MagicLink{}, err
	}
	row := m.db.QueryRow(sel, args...)
	var ml MagicLink
	err = row.Scan(&ml.ID, &ml.Token, &ml.UserID)
	return ml, err
}

func (m *MagicLinkRepoSql) Delete(id string) error {
	del, args, err := m.buildDelete(id)
	if err != nil {
		return err
	}
	_, err = m.db.Exec(del, args...)
	return err
}

func NewMagicLinkRepoSql(db *sql.DB, sbt squirrel.StatementBuilderType) MagicLinkRepo {
	_, err := db.Exec(createMagicLink)
	if err != nil {
		log.Fatalln("error creating magic link repo", err)
	}

	return &MagicLinkRepoSql{db: db, builder: builder{StatementBuilderType: sbt}}
}
