package scsadapter

import (
	"net/http"

	"github.com/alexedwards/scs/v2"
)

const (
	flashError   = "flasherror"
	flashSuccess = "flashsuccess"
)

type ScsAdapter struct {
	sessionManager *scs.SessionManager
}

func New(sessionManager *scs.SessionManager) *ScsAdapter {
	return &ScsAdapter{
		sessionManager: sessionManager,
	}
}

func (sa *ScsAdapter) SetErrorFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	sa.sessionManager.Put(r.Context(), flashError, msg)
	return nil
}

func (sa *ScsAdapter) SetSuccessFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	sa.sessionManager.Put(r.Context(), flashSuccess, msg)
	return nil
}

func (sa *ScsAdapter) GetFlashMessages(w http.ResponseWriter, r *http.Request) (e string, s string, err error) {
	errMsg := sa.sessionManager.PopString(r.Context(), flashError)
	successMsg := sa.sessionManager.PopString(r.Context(), flashSuccess)
	return errMsg, successMsg, nil
}

func (sa *ScsAdapter) SetValue(w http.ResponseWriter, r *http.Request, key, val string, ttl int) error {
	err := sa.sessionManager.RenewToken(r.Context())
	if err != nil {
		return err
	}

	sa.sessionManager.Put(r.Context(), key, val)
	//dur, durErr := time.ParseDuration(fmt.Sprintf("%ds", ttl))
	//if durErr == nil {
	//	sa.sessionManager.Lifetime = dur
	//} else {
	//	log.Println("error parsing duration", durErr)
	//}
	return nil
}

func (sa *ScsAdapter) GetValue(w http.ResponseWriter, r *http.Request, key string) (string, error) {
	val := sa.sessionManager.GetString(r.Context(), key)
	return val, nil
}

func (sa *ScsAdapter) ResetSession(w http.ResponseWriter, r *http.Request) error {
	sa.sessionManager.Destroy(r.Context())
	return nil
}
