package minotaur

type UserInfoRepo[R comparable, U UserInfo[R]] interface {
	LoadUserByEmail(email string) (U, error)
	// AuthCheck(email string) (string, string, error) // ID, password, error
	FindUser(id string) (U, error)
	UpdatePassword(id, password string) error
}
