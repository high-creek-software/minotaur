package minotaur

import (
	"github.com/rs/xid"
	"gitlab.com/high-creek-software/minotaur/usertoken"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"testing"
)

func TestAuthmanagerImpl(t *testing.T) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	if err != nil {
		t.Error("database setup failed", err)
	}

	repo := NewDefaultUserRepoGorm[string](db)

	id := xid.New()

	passEnc := BCryptPasswordEncoder{}
	pass, passErr := passEnc.Encode("abc123")
	if passErr != nil {
		t.Error("error encoding password", passErr)
	}

	user := DefaultUser[string]{
		ID:       id,
		Email:    "test@example.com",
		Name:     "Test Example",
		Role:     "admin",
		Password: pass,
	}

	repo.Store(user)

	userTokenRepo := usertoken.NewUserTokenRepoGorm(db)
	passResetRepo := NewPasswordResetRequestRepoImpl(db)

	authManager := NewAuthManager[string, DefaultUser[string]](repo, userTokenRepo, passEnc, passResetRepo)

	usr, token, err := authManager.TryLogin("test@example.com", "abc123", "TEST")
	if err != nil {
		t.Error("error logging in", err)
	}

	if usr.ID != id {
		t.Error("error ids don't match")
	}

	loadedUser, _, err := authManager.LoadUser(token.Token)
	if err != nil {
		t.Error("error loading user", err)
	}

	if loadedUser.ID != usr.ID {
		t.Error("error ids don't match")
	}
}
