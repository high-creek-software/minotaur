package minotaur

import (
	"log"
	"net/http"
)

const (
	defaultAuthSuccessRedirect = "/admin/dashboard"
	defaultLoginRoute          = "/login"

	defaultEmailFormKey    = "email"
	defaultPasswordFormKey = "password"

	defaultTokenQuery = "token"
	defaultIdQuery    = "id"
)

// AuthHandler is the entry point into the minotaur managed
type AuthHandler[R comparable, U UserInfo[R]] struct {
	*AuthMiddleware[R, U]
	sessionKey          string
	authSuccessRedirect string
	loginRedirect       string
	formEmailKey        string
	formPasswordKey     string
	tokenQueryKey       string
	idQueryKey          string
}

// NewAuthHandler constructs a new AuthHandler with the defaults
func NewAuthHandler[R comparable, U UserInfo[R]](authMiddleware *AuthMiddleware[R, U]) *AuthHandler[R, U] {
	ah := &AuthHandler[R, U]{
		AuthMiddleware:      authMiddleware,
		authSuccessRedirect: defaultAuthSuccessRedirect,
		loginRedirect:       defaultLoginRoute,
		formEmailKey:        defaultEmailFormKey,
		formPasswordKey:     defaultPasswordFormKey,
		tokenQueryKey:       defaultTokenQuery,
		idQueryKey:          defaultIdQuery,
	}

	return ah
}

// DoLogin this is a post endpoint
func (ah *AuthHandler[R, U]) DoLogin(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Println("error parsing login form", err)
		ah.setErrorFlash(w, r, "error logging in")
		ah.errorRedirect(w, r)
		return
	}

	email := r.FormValue(ah.formEmailKey)
	password := r.FormValue(ah.formPasswordKey)
	fingerprint := r.UserAgent()

	_, userToken, err := ah.authManager.TryLogin(email, password, fingerprint)
	if err != nil {
		log.Println("error trying login", err)
		ah.setErrorFlash(w, r, "error logging in")
		ah.errorRedirect(w, r)
		return
	}

	err = ah.SetTokenOnSession(w, r, userToken.Token)
	if err != nil {
		log.Println("error saving token to session", err)
		ah.setErrorFlash(w, r, "error logging in")
		ah.errorRedirect(w, r)
		return
	}

	http.Redirect(w, r, ah.authSuccessRedirect, http.StatusFound)
}

func (ah *AuthHandler[R, U]) DoInitiateMagicLinkLogin(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue(ah.formEmailKey)

	ah.authManager.InitiateLogin(email)
	ah.setSuccessFlash(w, r, "Check your email")

	http.Redirect(w, r, ah.loginRedirect, http.StatusFound)
}

func (ah *AuthHandler[R, U]) DoRedeemMagicLink(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get(ah.tokenQueryKey)
	id := r.URL.Query().Get(ah.idQueryKey)

	_, userToken, err := ah.authManager.RedeemLogin(token, id, r.UserAgent())
	if err != nil {
		log.Println("error redeeming magic link token", err)
		ah.setErrorFlash(w, r, "could not login")
		ah.errorRedirect(w, r)
		return
	}

	err = ah.SetTokenOnSession(w, r, userToken.Token)
	if err != nil {
		log.Println("error saving token to session", err)
		ah.setErrorFlash(w, r, "error logging in")
		ah.errorRedirect(w, r)
		return
	}

	http.Redirect(w, r, ah.authSuccessRedirect, http.StatusFound)
}

func (ah *AuthHandler[R, U]) RedirectAuthedMiddleware(path string) func(handler http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			if token, tokErr := ah.sessionAdapter.GetValue(w, r, ah.tokenKey); token != "" && tokErr == nil {
				http.Redirect(w, r, path, http.StatusFound)
				return
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (ah *AuthHandler[R, U]) SetAuthSuccessRoute(successRoute string) *AuthHandler[R, U] {
	ah.authSuccessRedirect = successRoute

	return ah
}

func (ah *AuthHandler[R, U]) SetLoginRedirect(loginRedirect string) *AuthHandler[R, U] {
	ah.loginRedirect = loginRedirect

	return ah
}

func (ah *AuthHandler[R, U]) SetFormEmailKey(emailKey string) *AuthHandler[R, U] {
	ah.formEmailKey = emailKey

	return ah
}

func (ah *AuthHandler[R, U]) SetFormPasswordKey(passwordKey string) *AuthHandler[R, U] {
	ah.formPasswordKey = passwordKey

	return ah
}

func (ah *AuthHandler[R, U]) SetTokenQueryKey(tokenQueryKey string) *AuthHandler[R, U] {
	ah.tokenQueryKey = tokenQueryKey

	return ah
}

func (ah *AuthHandler[R, U]) SetIdQueryKey(idQueryKey string) *AuthHandler[R, U] {
	ah.idQueryKey = idQueryKey

	return ah
}
