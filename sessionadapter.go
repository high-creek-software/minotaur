package minotaur

import "net/http"

type SessionAdapter interface {
	SetErrorFlash(w http.ResponseWriter, r *http.Request, msg string) error
	SetSuccessFlash(w http.ResponseWriter, r *http.Request, msg string) error
	SetValue(w http.ResponseWriter, r *http.Request, key, val string, ttl int) error
	GetValue(w http.ResponseWriter, r *http.Request, key string) (string, error)
	ResetSession(w http.ResponseWriter, r *http.Request) error
}
