package defaultuser

import (
	"encoding/json"
	"io"
	"strings"

	"github.com/rs/xid"
	"gitlab.com/high-creek-software/minotaur/passwords"
)

type UserManager[R comparable] interface {
	CreateUser(email, name string, role R, password string) (User[R], error)
	StoreUser(r io.Reader) (User[R], error)
	UpdateUser(r io.Reader) error
	UpdatePassword(id, password, verify string) error
	FindAndSanitizeUser(id string) (User[R], error)
	UserCount() (int64, error)
}

type UserManagerImpl[R comparable] struct {
	repo            UserRepo[R]
	passwordEncoder passwords.PasswordEncoder
}

func NewUserManagerImpl[R comparable](repo UserRepo[R], passwordEncoder passwords.PasswordEncoder) UserManager[R] {
	return &UserManagerImpl[R]{repo: repo, passwordEncoder: passwordEncoder}
}

func (dm *UserManagerImpl[R]) CreateUser(email, name string, role R, password string) (User[R], error) {
	id := xid.New()
	pass, passErr := dm.passwordEncoder.Encode(password)
	if passErr != nil {
		return User[R]{}, passErr
	}
	du := User[R]{ID: id, Email: strings.ToLower(email), Name: name, Role: role, Password: pass}

	storeErr := dm.repo.Store(du)
	return du, storeErr
}

func (dm *UserManagerImpl[R]) StoreUser(r io.Reader) (User[R], error) {
	id := xid.New()

	var du User[R]
	decodeErr := json.NewDecoder(r).Decode(&du)
	if decodeErr != nil {
		return User[R]{}, decodeErr
	}

	pass, passErr := dm.passwordEncoder.Encode(du.Password)
	if passErr != nil {
		return User[R]{}, passErr
	}

	du.Password = pass
	du.ID = id
	storeErr := dm.repo.Store(du)
	return du, storeErr
}

func (dm *UserManagerImpl[R]) UpdateUser(r io.Reader) error {
	var du User[R]
	decodeErr := json.NewDecoder(r).Decode(&du)
	if decodeErr != nil {
		return decodeErr
	}

	return dm.repo.UpdateUser(du)
}

func (dm *UserManagerImpl[R]) UpdatePassword(id, password, verify string) error {
	if password != verify {
		return passwords.ErrorPasswordsMustMatch
	}

	pass, passErr := dm.passwordEncoder.Encode(password)
	if passErr != nil {
		return passErr
	}

	return dm.repo.UpdatePassword(id, pass)
}

func (dm *UserManagerImpl[R]) FindAndSanitizeUser(id string) (User[R], error) {
	usr, err := dm.repo.FindUser(id)
	if err != nil {
		return User[R]{}, err
	}
	usr.Password = ""

	return usr, nil
}

func (dm *UserManagerImpl[R]) UserCount() (int64, error) {
	return dm.repo.UserCount()
}
