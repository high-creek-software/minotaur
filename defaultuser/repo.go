package defaultuser

type UserRepo[R comparable] interface {
	LoadUserByEmail(email string) (User[R], error)
	FindUser(id string) (User[R], error)
	Store(defUser User[R]) error
	UpdateUser(defuser User[R]) error
	UpdatePassword(id, password string) error
	UserCount() (int64, error)
	ListUsers() ([]User[R], error)
	DeleteUser(id string) error
}
