package defaultuser

import (
	"github.com/Masterminds/squirrel"
	"github.com/rs/xid"
)

type builder[R comparable] struct {
	squirrel.StatementBuilderType
}

func (b builder[R]) buildLoadUserByEmail(email string) (string, []any, error) {
	return b.Select("id", "email", "name", "role", "password", "avatar", "bio").
		From("users").
		Where(squirrel.Eq{"email": email}).
		ToSql()
}

func (b builder[R]) buildAuthCheck(email string) (string, []any, error) {
	return b.Select("id", "password").
		From("users").
		Where(squirrel.Eq{"email": email}).
		ToSql()
}

func (b builder[R]) buildFindUser(id string) (string, []any, error) {
	return b.Select("id", "email", "name", "role", "avatar", "bio").
		From("users").
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder[R]) buildListUsers() (string, []any, error) {
	return b.Select("id", "email", "name", "role", "avatar", "bio").
		From("users").
		ToSql()
}

func (b builder[R]) buildStoreUser(id xid.ID, email, name, password, avatar, bio string, role R) (string, []any, error) {
	return b.Insert("users").
		Columns("id", "email", "name", "role", "password", "avatar", "bio").
		Values(id, email, name, role, password, avatar, bio).
		ToSql()
}

func (b builder[R]) buildUpdateUser(id xid.ID, email, name, avatar, bio string) (string, []any, error) {
	return b.Update("users").
		Set("email", email).
		Set("name", name).
		Set("avatar", avatar).
		Set("bio", bio).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder[R]) buildUpdatePassword(id, password string) (string, []any, error) {
	return b.Update("users").
		Set("password", password).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder[R]) buildDeleteUser(id string) (string, []any, error) {
	return b.Delete("users").
		Where(squirrel.Eq{"id": id}).
		ToSql()
}
