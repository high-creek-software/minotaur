package defaultuser

import (
	"database/sql"
	"log"
	"log/slog"
	"strings"

	"github.com/Masterminds/squirrel"
)

const (
	createTableSqlite = `CREATE TABLE IF NOT EXISTS users(
		id varchar(36) primary key,
		email varchar(1024),
		"name" varchar(1024),
		"role" varchar(1024),
		password varchar(512),
		avatar varchar(2048),
		bio text,
    	created_at timestamp,
    	updated_at timestamp
	);`

	createEmailIndex = `CREATE INDEX IF NOT EXISTS user_email_idx ON users(email);`
)

type UserRepoSql[R comparable] struct {
	builder[R]
	db *sql.DB
}

func NewUserRepoSql[R comparable](db *sql.DB, sbt squirrel.StatementBuilderType) *UserRepoSql[R] {
	repo := &UserRepoSql[R]{db: db, builder: builder[R]{StatementBuilderType: sbt}}
	repo.CreateTable()
	return repo
}

func (d UserRepoSql[R]) CreateTable() {
	_, err := d.db.Exec(createTableSqlite)
	if err != nil {
		log.Fatalln("error creating users table", err)
	}

	_, err = d.db.Exec(createEmailIndex)
	if err != nil && !strings.Contains(err.Error(), "index user_email_idx already exists") {
		log.Fatalln("error creating email index", err)
	}
}

func (d UserRepoSql[R]) LoadUserByEmail(email string) (User[R], error) {
	var usr User[R]
	sel, args, err := d.buildLoadUserByEmail(email)
	if err != nil {
		return usr, err
	}
	row := d.db.QueryRow(sel, args...)

	err = row.Scan(&usr.ID, &usr.Email, &usr.Name, &usr.Role, &usr.Password, &usr.Avatar, &usr.Bio)

	return usr, err
}

func (d UserRepoSql[R]) AuthCheck(email string) (string, string, error) {
	sel, args, err := d.buildAuthCheck(email)
	if err != nil {
		return "", "", err
	}

	row := d.db.QueryRow(sel, args...)

	var id string
	var password string

	err = row.Scan(&id, &password)

	return id, password, err
}

func (d UserRepoSql[R]) FindUser(id string) (User[R], error) {
	var usr User[R]
	sel, args, err := d.buildFindUser(id)
	if err != nil {
		return usr, err
	}
	row := d.db.QueryRow(sel, args...)

	err = row.Scan(&usr.ID, &usr.Email, &usr.Name, &usr.Role, &usr.Avatar, &usr.Bio)

	return usr, err
}

func (d UserRepoSql[R]) ListUsers() ([]User[R], error) {
	sel, args, err := d.buildListUsers()
	if err != nil {
		return nil, err
	}

	rows, err := d.db.Query(sel, args...)
	if err != nil {
		return nil, err
	}

	var usrs []User[R]
	for rows.Next() {
		var u User[R]
		if err := rows.Scan(&u.ID, &u.Email, &u.Name, &u.Role, &u.Avatar, &u.Bio); err == nil {
			usrs = append(usrs, u)
		} else {
			slog.Error("error scanning user", "error", err)
		}
	}

	return usrs, nil
}

func (d UserRepoSql[R]) Store(defUser User[R]) error {
	ins, args, err := d.buildStoreUser(defUser.ID, defUser.Email, defUser.Name, defUser.Password, defUser.Avatar, defUser.Bio, defUser.Role)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(ins, args...)

	return err
}

func (d UserRepoSql[R]) UpdateUser(defuser User[R]) error {
	upd8, args, err := d.buildUpdateUser(defuser.ID, defuser.Email, defuser.Name, defuser.Avatar, defuser.Bio)
	if err != nil {
		return err
	}

	_, err = d.db.Exec(upd8, args...)

	return err
}

func (d UserRepoSql[R]) UpdatePassword(id, password string) error {
	upd8, args, err := d.buildUpdatePassword(id, password)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(upd8, args...)
	return err
}

func (d UserRepoSql[R]) UserCount() (int64, error) {
	var count int64
	row := d.db.QueryRow("SELECT count(*) FROM users;")

	err := row.Scan(&count)
	return count, err
}

func (d UserRepoSql[R]) DeleteUser(id string) error {
	del, args, err := d.buildDeleteUser(id)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(del, args...)

	return err
}
