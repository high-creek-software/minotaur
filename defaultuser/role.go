package defaultuser

import (
	"encoding/json"
	"strings"
)

type Role int64

const (
	RoleUnkown Role = iota
	RoleUser
	RoleAdmin
)

var defaultRoleNames = []string{"Unknown", "User", "Admin"}

func (dr Role) String() string {
	return defaultRoleNames[dr]
}

func (dr Role) MarshalJSON() ([]byte, error) {
	return json.Marshal(dr.String())
}

func (dr *Role) UnmarshalJSON(b []byte) error {
	roleStr := strings.Trim(string(b), `"`)
	*dr = RoleFromString(roleStr)
	return nil
}

func RoleFromString(input string) Role {
	for idx, r := range defaultRoleNames {
		if input == r {
			return Role(idx)
		}
	}

	return RoleUnkown
}
