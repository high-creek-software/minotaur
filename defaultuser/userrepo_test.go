package defaultuser

import (
	"database/sql"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/xid"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestDefaultUserRepoGormInstantiate(t *testing.T) {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	//db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		t.Error("Database setup should not have an error", err)
	}
	repo := NewDefaultUserRepoGorm[string](db)

	id := xid.New()
	user := User[string]{
		ID:       id,
		Email:    "test@example.com",
		Name:     "Test Example",
		Role:     "admin",
		Password: "abc123",
	}

	storeErr := repo.Store(user)
	if storeErr != nil {
		t.Error("error storing user", storeErr)
	}

	usr, usrErr := repo.LoadUserByEmail("test@example.com")

	if usrErr != nil {
		t.Error("error finding user", usrErr)
	}

	if usr.GetEmail() != "test@example.com" {
		t.Error("users did not match")
	}

	if usr.GetID() != id.String() {
		t.Error("ids did not match")
	}
}

func TestDefaultUSerRepoSqlInstantiate(t *testing.T) {
	db, err := sql.Open("sqlite3", "file::memory:?cache=shared")
	if err != nil {
		t.Error("database setup failed", err)
	}
	repo := NewDefaultUserRepoSql[Role](db)
	repo.CreateTable()

	id := xid.New()
	admin := RoleAdmin
	user := User[Role]{
		ID:       id,
		Email:    "test@example.com",
		Name:     "Test Example",
		Role:     admin,
		Password: "abc123",
	}

	storeErr := repo.Store(user)
	if storeErr != nil {
		t.Error("error storing user", storeErr)
	}

	usr, usrErr := repo.LoadUserByEmail("test@example.com")

	if usrErr != nil {
		t.Error("error finding user", usrErr)
	}

	if usr.GetEmail() != "test@example.com" {
		t.Error("users did not match")
	}

	if usr.GetID() != id.String() {
		t.Error("ids did not match")
	}

}
