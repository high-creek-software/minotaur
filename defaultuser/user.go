package defaultuser

import (
	"time"

	"github.com/rs/xid"
)

type User[R comparable] struct {
	ID        xid.ID     `json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"UpdatedAt"`
	DeletedAt *time.Time `sql:"index" json:"deletedAt"`
	Email     string     `gorm:"index:user_email_idx" json:"email"`
	Name      string     `json:"name"`
	Role      R          `json:"role"`
	Password  string     `json:"-"`
	Avatar    string     `json:"avatar"`
	Bio       string     `gorm:"type:text" json:"bio"`
}

func (User[R]) TableName() string {
	return "users"
}

func (d User[R]) GetID() string {
	return d.ID.String()
}

func (d User[R]) GetEmail() string {
	return d.Email
}

func (d User[R]) GetName() string {
	return d.Name
}

func (d User[R]) GetRoles() []R {
	return []R{d.Role}
}

func (d User[R]) GetPassword() string {
	return d.Password
}

func (d User[R]) IsAccountViable() bool {
	return true
}

func (d User[R]) AreCredentialsViable() bool {
	return true
}

func (d User[R]) IsEnabled() bool {
	return true
}

//func (d DefaultUser[R]) String() string {
//	sb := strings.Builder{}
//	sb.WriteString(fmt.Sprintf("ID: %s \n", d.ID))
//	sb.WriteString(fmt.Sprintf("Email: %s \n", d.Email))
//	sb.WriteString(fmt.Sprintf("Name: %s \n", d.Name))
//	sb.WriteString(fmt.Sprintf("Role: %s", d.Role))
//
//	return sb.String()
//}
