package defaultuser

import (
	"log"

	"gorm.io/gorm"
)

var _ UserRepo[int] = (*UserRepoGorm[int])(nil)

type UserRepoGorm[R comparable] struct {
	db *gorm.DB
}

func NewDefaultUserRepoGorm[R comparable](db *gorm.DB) *UserRepoGorm[R] {
	userRepo := &UserRepoGorm[R]{db: db}
	err := db.AutoMigrate(&User[R]{})
	if err != nil {
		log.Fatalln(err)
	}
	return userRepo
}

func (d UserRepoGorm[R]) LoadUserByEmail(email string) (User[R], error) {
	var defUser User[R]
	err := d.db.First(&defUser, "email = ?", email).Error
	return defUser, err
}

func (d UserRepoGorm[R]) AuthCheck(email string) (string, string, error) {
	usr, err := d.LoadUserByEmail(email)
	if err != nil {
		return "", "", err
	}

	return usr.ID.String(), usr.Password, nil
}

func (d UserRepoGorm[R]) FindUser(id string) (User[R], error) {
	var defUser User[R]
	err := d.db.First(&defUser, "id = ?", id).Error
	if err != nil {
		return defUser, err
	}
	defUser.Password = ""
	return defUser, nil
}

func (d UserRepoGorm[R]) Store(defUser User[R]) error {
	err := d.db.Create(&defUser).Error
	return err
}

func (d UserRepoGorm[R]) UpdateUser(defUser User[R]) error {
	update := map[string]interface{}{"email": defUser.Email, "name": defUser.Name, "role": defUser.Role, "avatar": defUser.Avatar, "bio": defUser.Bio}
	return d.db.Model(&defUser).Updates(update).Error
}

func (d UserRepoGorm[R]) UpdatePassword(id, password string) error {
	return d.db.Model(&User[R]{}).Where("id = ?", id).Update("password", password).Error
}

func (d UserRepoGorm[R]) UserCount() (int64, error) {
	var count int64
	err := d.db.Model(&User[R]{}).Count(&count).Error
	return count, err
}

func (d UserRepoGorm[R]) ListUsers() ([]User[R], error) {
	panic("not yet implemented")
}

func (d UserRepoGorm[R]) DeleteUser(id string) error {
	panic("not yet implemented")
}
