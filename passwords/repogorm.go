package passwords

import (
	"github.com/rs/xid"
	"gorm.io/gorm"
	"log"
)

type PasswordResetRequestRepoImpl struct {
	db *gorm.DB
}

func NewPasswordResetRequestRepoImpl(db *gorm.DB) PasswordResetRequestRepo {
	err := db.AutoMigrate(&PasswordResetRequest{})
	if err != nil {
		log.Fatalln(err)
	}
	return &PasswordResetRequestRepoImpl{db: db}
}

func (repo *PasswordResetRequestRepoImpl) Store(prr PasswordResetRequest) error {
	return repo.db.Create(&prr).Error
}

func (repo *PasswordResetRequestRepoImpl) Find(id xid.ID) (PasswordResetRequest, error) {
	var prr PasswordResetRequest
	err := repo.db.Find(&prr, "id = ?", id).Error
	return prr, err
}
