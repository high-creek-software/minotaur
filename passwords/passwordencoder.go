package passwords

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
)

var ErrorPasswordsMustMatch = errors.New("passwords must match")

type PasswordEncoder interface {
	Encode(rawPassword string) (string, error)
	Matches(rawPassword, encodedPassword string) error
}

type BCryptPasswordEncoder struct {
}

func (B BCryptPasswordEncoder) Encode(rawPassword string) (string, error) {
	res, err := bcrypt.GenerateFromPassword([]byte(rawPassword), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(res), nil
}

func (B BCryptPasswordEncoder) Matches(rawPassword, encodedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(encodedPassword), []byte(rawPassword))
}
