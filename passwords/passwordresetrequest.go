package passwords

import (
	"time"

	"github.com/rs/xid"
)

type PasswordResetRequest struct {
	ID        xid.ID `json:"id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
	Email     string     `json:"email"`
	UserID    string
	Token     string
}

func (PasswordResetRequest) TableName() string {
	return "password_reset_request"
}
