package passwords

import (
	"github.com/Masterminds/squirrel"
	"github.com/rs/xid"
)

const (
	prrName = "password_reset_request"
)

type builder struct {
	squirrel.StatementBuilderType
}

func (b builder) buildInsert(prr PasswordResetRequest) (string, []any, error) {
	return b.Insert(prrName).
		Columns("id", "created_at", "updated_at", "email", "user_id", "token").
		Values(prr.ID, prr.CreatedAt, prr.UpdatedAt, prr.Email, prr.UserID, prr.Token).
		ToSql()
}

func (b builder) buildFind(id xid.ID) (string, []any, error) {
	return b.Select("id", "create_at", "updated_at", "email", "user_id", "token").
		From(prrName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}
