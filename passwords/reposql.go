package passwords

import (
	"database/sql"
	"github.com/Masterminds/squirrel"
	"github.com/rs/xid"
	"log"
)

const (
	createResetTable = `CREATE TABLE IF NOT EXISTS password_reset_request(
		id varchar(36) primary key,
		created_at datetime,
		updated_at datetime,
		email varchar(512),
		user_id varchar(36),
		token text
	);`
)

type PasswordResetRequestRepoSql struct {
	builder
	db *sql.DB
}

func NewPasswordResetRequestRepoSql(db *sql.DB, sbt squirrel.StatementBuilderType) PasswordResetRequestRepo {
	_, err := db.Exec(createResetTable)
	if err != nil {
		log.Fatalln("error creating password reset request table", err)
	}

	return &PasswordResetRequestRepoSql{
		db:      db,
		builder: builder{StatementBuilderType: sbt},
	}
}

func (r *PasswordResetRequestRepoSql) Store(prr PasswordResetRequest) error {
	ins, args, err := r.buildInsert(prr)
	if err != nil {
		return err
	}

	_, err = r.db.Exec(ins, args...)
	return err
}

func (r *PasswordResetRequestRepoSql) Find(id xid.ID) (PasswordResetRequest, error) {
	q, args, err := r.buildFind(id)
	if err != nil {
		return PasswordResetRequest{}, err
	}

	row := r.db.QueryRow(q, args...)

	var prr PasswordResetRequest
	err = row.Scan(&prr.ID, &prr.CreatedAt, &prr.UpdatedAt, &prr.Email, &prr.UserID, &prr.Token)

	return prr, err
}
