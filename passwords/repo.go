package passwords

import "github.com/rs/xid"

type PasswordResetRequestRepo interface {
	Store(prr PasswordResetRequest) error
	Find(id xid.ID) (PasswordResetRequest, error)
}
