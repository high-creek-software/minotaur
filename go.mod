module gitlab.com/high-creek-software/minotaur

go 1.18

require (
	github.com/mattn/go-sqlite3 v1.14.12
	github.com/rs/xid v1.5.0
	golang.org/x/crypto v0.26.0
	gorm.io/driver/sqlite v1.3.2
	gorm.io/gorm v1.25.11
)

require (
	github.com/Masterminds/squirrel v1.5.4 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	golang.org/x/text v0.17.0 // indirect
)
