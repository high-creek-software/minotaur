package usertoken

import (
	"github.com/Masterminds/squirrel"
	"time"
)

type builder struct {
	squirrel.StatementBuilderType
}

func (b builder) buildStore(token Token) (string, []any, error) {
	now := time.Now()
	return b.Insert(tokenName).
		Columns("id", "user_id", "token", "fingerprint", "created_at", "updated_at").
		Values(token.ID, token.UserID, token.Token, token.Fingerprint, now, now).
		ToSql()
}

func (b builder) buildFindByID(id string) (string, []any, error) {
	return b.Select("id", "user_id", "token", "fingerprint", "created_at", "updated_at").
		From(tokenName).
		Where(squirrel.Eq{"id": id}).
		ToSql()
}

func (b builder) buildFindByToken(token string) (string, []any, error) {
	return b.Select("id", "user_id", "token", "fingerprint", "created_at", "updated_at").
		From(tokenName).
		Where(squirrel.Eq{"token": token}).
		ToSql()
}

func (b builder) buildDeleteToken(token string) (string, []any, error) {
	return b.Delete(tokenName).
		Where(squirrel.Eq{"token": token}).
		ToSql()
}

func (b builder) buildDeleteTokenByID(tokenID string) (string, []any, error) {
	return b.Delete(tokenName).
		Where(squirrel.Eq{"id": tokenID}).
		ToSql()
}

func (b builder) buildListUserTokens(userID string) (string, []any, error) {
	return b.Select("id", "user_id", "token", "fingerprint").
		From(tokenName).
		Where(squirrel.Eq{"user_id": userID}).
		ToSql()
}

func (b builder) buildDeleteAllForUser(userID string) (string, []any, error) {
	return b.Delete(tokenName).
		Where(squirrel.Eq{"user_id": userID}).
		ToSql()
}
