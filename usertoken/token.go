package usertoken

import (
	"context"
	"gitlab.com/high-creek-software/minotaur/key"
	"net/http"
	"time"

	"github.com/rs/xid"
)

type Token struct {
	ID          xid.ID `json:"id"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time `sql:"index"`
	UserID      string     `json:"userId" gorm:"index:usertoken_userid_idx"`
	Token       string     `json:"token" gorm:"index:usertoken_token_idx"`
	Fingerprint string     `json:"fingerprint"`
}

var tokenKey = key.Key(24)

func SetToken(token string, r *http.Request) {
	ctx := r.Context()
	ctx = context.WithValue(ctx, tokenKey, token)
	*r = *(r.WithContext(ctx))
}

func TokenFromContext(r *http.Request) (string, bool) {
	token, ok := r.Context().Value(tokenKey).(string)
	return token, ok
}

func (Token) TableName() string {
	return "user_tokens"
}
