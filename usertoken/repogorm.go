package usertoken

import (
	"github.com/rs/xid"
	"gorm.io/gorm"
	"log"
)

type TokenRepoGorm struct {
	db *gorm.DB
}

func NewTokenRepoGorm(db *gorm.DB) *TokenRepoGorm {
	userRepo := &TokenRepoGorm{db: db}
	err := db.AutoMigrate(&Token{})
	if err != nil {
		log.Fatalln(err)
	}
	return userRepo
}

func (u *TokenRepoGorm) Store(userToken Token) error {
	err := u.db.Create(&userToken).Error
	return err
}

func (u *TokenRepoGorm) FindByID(tokenID string) (Token, error) {
	var ut Token
	id, err := xid.FromString(tokenID)
	if err != nil {
		return Token{}, err
	}
	err = u.db.First(&ut, "id = ?", id).Error
	return ut, err
}

func (u *TokenRepoGorm) FindByToken(token string) (Token, error) {
	var ut Token
	err := u.db.First(&ut, "token = ?", token).Error
	return ut, err
}

func (u *TokenRepoGorm) DeleteToken(token string) error {
	return u.db.Where("token = ?", token).Delete(&Token{}).Error
}

func (u *TokenRepoGorm) DeleteTokenById(tokenID string) error {
	id, err := xid.FromString(tokenID)
	if err != nil {
		return err
	}
	return u.db.Where("id = ?", id).Delete(&Token{}).Error
}

func (u *TokenRepoGorm) ListUserTokens(userID string) ([]Token, error) {
	var tokens []Token
	err := u.db.Where(&Token{UserID: userID}).First(&tokens).Error
	return tokens, err
}

func (u *TokenRepoGorm) DeleteAllForUser(userID string) error {
	panic("not yet implemented")
}
