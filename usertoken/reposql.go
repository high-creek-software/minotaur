package usertoken

import (
	"database/sql"
	"github.com/Masterminds/squirrel"
	"log"
	"log/slog"
)

const (
	createToken = `CREATE TABLE IF NOT EXISTS user_token(
    id varchar(36) primary key,
    user_id varchar(36) not null,
    token varchar(256),
    fingerprint text,
    created_at timestamp,
    updated_at timestamp
);`

	createIdIdx = `CREATE INDEX IF NOT EXISTS usertoken_userid_idx ON user_token(user_id)`

	createTokenIdx = `CREATE INDEX IF NOT EXISTS usertoken_token_idx ON user_token(token);`

	tokenName = "user_token"
)

type TokenRepoSql struct {
	builder
	db *sql.DB
}

func (u TokenRepoSql) Store(userToken Token) error {
	ins, args, err := u.buildStore(userToken)
	if err != nil {
		return err
	}
	_, err = u.db.Exec(ins, args...)
	return err
}

func (u TokenRepoSql) FindByID(tokenID string) (Token, error) {
	sel, args, err := u.buildFindByID(tokenID)
	if err != nil {
		return Token{}, err
	}
	row := u.db.QueryRow(sel, args...)

	var userToken Token
	err = row.Scan(&userToken.ID, &userToken.UserID, &userToken.Token, &userToken.Fingerprint, &userToken.CreatedAt, &userToken.UpdatedAt)

	return userToken, err
}

func (u TokenRepoSql) FindByToken(token string) (Token, error) {
	sel, args, err := u.buildFindByToken(token)
	if err != nil {
		return Token{}, err
	}
	row := u.db.QueryRow(sel, args...)

	var userToken Token
	err = row.Scan(&userToken.ID, &userToken.UserID, &userToken.Token, &userToken.Fingerprint, &userToken.CreatedAt, &userToken.UpdatedAt)

	return userToken, err
}

func (u TokenRepoSql) DeleteToken(token string) error {
	del, args, err := u.buildDeleteToken(token)
	if err != nil {
		return err
	}
	_, err = u.db.Exec(del, args...)
	return err
}

func (u TokenRepoSql) DeleteTokenById(tokenID string) error {
	del, args, err := u.buildDeleteTokenByID(tokenID)
	if err != nil {
		return err
	}
	_, err = u.db.Exec(del, args...)
	return err
}

func (u TokenRepoSql) ListUserTokens(userID string) ([]Token, error) {
	sel, args, err := u.buildListUserTokens(userID)
	if err != nil {
		return nil, err
	}

	rows, err := u.db.Query(sel, args...)
	if err != nil {
		return nil, err
	}

	var results []Token
	for rows.Next() {
		var res Token
		if err := rows.Scan(&res.ID, &res.UserID, &res.Token, &res.Fingerprint); err == nil {
			results = append(results, res)
		} else {
			slog.Error("error scanning user token", err)
		}
	}

	return results, nil
}

func (u TokenRepoSql) DeleteAllForUser(userID string) error {
	del, args, err := u.buildDeleteAllForUser(userID)
	if err != nil {
		return err
	}
	_, err = u.db.Exec(del, args...)
	return err
}

func NewTokenRpoSql(db *sql.DB, sbt squirrel.StatementBuilderType) Repo {
	_, err := db.Exec(createToken)
	if err != nil {
		log.Fatalln("error creating user_token table", err)
	}

	_, err = db.Exec(createIdIdx)
	if err != nil {
		log.Fatalln("error creating user token index", err)
	}

	_, err = db.Exec(createTokenIdx)
	if err != nil {
		log.Fatalln("error creating user token token index", err)
	}

	return &TokenRepoSql{db: db, builder: builder{StatementBuilderType: sbt}}
}
