package usertoken

type Repo interface {
	Store(userToken Token) error
	FindByID(tokenID string) (Token, error)
	FindByToken(token string) (Token, error)
	DeleteToken(token string) error
	DeleteTokenById(tokenID string) error
	ListUserTokens(userID string) ([]Token, error)
	DeleteAllForUser(userID string) error
}
