package savingsender

import (
	"github.com/Masterminds/squirrel"
	"github.com/rs/xid"
)

type builder struct {
	squirrel.StatementBuilderType
}

func newBuilder(sb squirrel.StatementBuilderType) builder {
	return builder{StatementBuilderType: sb}
}

func (b builder) buildInsertMessage(savedMessage SavedMessage) (string, []any, error) {
	return b.Insert("messages").
		Columns(
			"id",
			"at",
			"target_email",
			"subject",
			"message",
		).
		Values(
			savedMessage.ID,
			savedMessage.At,
			savedMessage.TargetEmail,
			savedMessage.Subject,
			savedMessage.Message,
		).
		ToSql()
}

func (b builder) buildFindByEmail(email, subject string) (string, []any, error) {
	build := b.Select(
		"id",
		"at",
		"target_email",
		"subject",
		"message",
	).
		From("messages").
		Where(squirrel.Eq{"target_email": email})

	if subject != "" {
		build = build.Where(squirrel.Eq{"subject": subject})
	}

	return build.ToSql()
}

func (b builder) buildDeleteById(id xid.ID) (string, []any, error) {
	return b.Delete("messages").Where(squirrel.Eq{"id": id}).ToSql()
}

func (b builder) buildDeleteByEmailAndSubject(email, subject string) (string, []any, error) {
	return b.Delete("messages").
		Where(squirrel.Eq{"target_email": email, "subject": subject}).
		ToSql()
}
