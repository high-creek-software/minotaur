package savingsender

import (
	"time"

	"github.com/rs/xid"
)

type SavedMessage struct {
	ID          xid.ID    `json:"id"`
	At          time.Time `json:"at"`
	TargetEmail string    `json:"targetEmail"`
	Subject     string    `json:"subject"`
	Message     string    `json:"message"`
}
