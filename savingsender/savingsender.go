package savingsender

import (
	"database/sql"
	"log"
	"log/slog"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/rs/xid"
	"gitlab.com/high-creek-software/minotaur/messaging"
)

const (
	createSavingSenderTable = `CREATE TABLE IF NOT EXISTS messages(
		id VARCHAR(26) PRIMARY KEY,
		at TIMESTAMP,
		target_email TEXT,
		subject TEXT,
		message TEXT
	);`
)

type SavingSender struct {
	db *sql.DB
	builder
}

func NewSavingSender(db *sql.DB, sb squirrel.StatementBuilderType) *SavingSender {
	_, err := db.Exec(createSavingSenderTable)
	if err != nil {
		log.Fatalln("error creating messages table", err)
	}

	return &SavingSender{db: db, builder: newBuilder(sb)}
}

func (s *SavingSender) Send(to, from messaging.Email, message messaging.Message) error {
	savedMessage := SavedMessage{
		ID:          xid.New(),
		At:          time.Now(),
		TargetEmail: to.GetAddress(),
		Subject:     message.GetSubject(),
		Message:     message.GetPlain(),
	}

	ins, args, err := s.builder.buildInsertMessage(savedMessage)
	if err != nil {
		return err
	}

	_, err = s.db.Exec(ins, args...)
	return err
}

func (s *SavingSender) FindByTarget(targetEmail, subject string) ([]SavedMessage, error) {
	sel, args, err := s.builder.buildFindByEmail(targetEmail, subject)
	if err != nil {
		return nil, err
	}

	rows, err := s.db.Query(sel, args...)
	if err != nil {
		return nil, err
	}

	var msgs []SavedMessage
	for rows.Next() {
		var msg SavedMessage
		if err := rows.Scan(&msg.ID, &msg.At, &msg.TargetEmail, &msg.Subject, &msg.Message); err == nil {
			msgs = append(msgs, msg)
		} else {
			slog.Error("error scanning saved message", "error", err)
		}
	}

	return msgs, nil
}

func (s *SavingSender) DeleteByID(id xid.ID) error {
	del, args, err := s.builder.buildDeleteById(id)
	if err != nil {
		return err
	}

	_, err = s.db.Exec(del, args...)
	return err
}

func (s *SavingSender) DeleteForEmailAndSubject(email, subject string) error {
	del, args, err := s.builder.buildDeleteByEmailAndSubject(email, subject)
	if err != nil {
		return err
	}

	_, err = s.db.Exec(del, args...)

	return err
}
