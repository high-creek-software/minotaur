package minotaur

import (
	"context"
	"gitlab.com/high-creek-software/minotaur/key"
	"net/http"
)

type UserInfo[R comparable] interface {
	GetID() string
	GetEmail() string
	GetName() string
	GetRoles() []R
	GetPassword() string
	IsAccountViable() bool
	AreCredentialsViable() bool
	IsEnabled() bool
}

var userInfoKey = key.Key(22)

func SetUserInfo[R comparable, U UserInfo[R]](userInfo U, r *http.Request) {
	ctx := r.Context()
	ctx = context.WithValue(ctx, userInfoKey, userInfo)
	*r = *(r.WithContext(ctx))
}

func UserInfoFromContext[R comparable, U UserInfo[R]](r *http.Request) (U, bool) {
	userInfo, ok := r.Context().Value(userInfoKey).(U)
	return userInfo, ok
}
