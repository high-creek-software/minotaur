package gorillaadapter

import (
	"net/http"

	"github.com/gorilla/sessions"
)

const (
	flashSession      = "flash"
	flashError        = "flasherror"
	flashSuccess      = "flashsuccess"
	defaultSessionKey = "minotaur-session"
)

type GorillaConfig func(adapter *GorillaAdapter)

type GorillaAdapter struct {
	store      sessions.Store
	sessionKey string
}

func New(store sessions.Store, opts ...GorillaConfig) *GorillaAdapter {
	ga := &GorillaAdapter{store: store, sessionKey: defaultSessionKey}
	for _, opt := range opts {
		opt(ga)
	}
	return ga
}

func (ga *GorillaAdapter) SetErrorFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	return ga.setFlashMessage(w, r, flashError, msg)
}

func (ga *GorillaAdapter) SetSuccessFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	return ga.setFlashMessage(w, r, flashSuccess, msg)
}

func (ga *GorillaAdapter) GetFlashMessages(w http.ResponseWriter, r *http.Request) (string, string, error) {
	var errFlash string
	var successFlash string
	if sess, sessErr := ga.store.Get(r, flashSession); sessErr == nil {
		errArr := sess.Flashes(flashError)
		if len(errArr) > 0 {
			errFlash = errArr[0].(string)
		}
		successArr := sess.Flashes(flashSuccess)
		if len(successArr) > 0 {
			successFlash = successArr[0].(string)
		}
		sess.Options.MaxAge = -1
		sess.Save(r, w)
	} else {
		return errFlash, successFlash, sessErr
	}
	return errFlash, successFlash, nil
}

func (ga *GorillaAdapter) setFlashMessage(w http.ResponseWriter, r *http.Request, key, message string) error {
	if ga.store == nil {
		return nil
	}
	if sess, sessErr := ga.store.Get(r, flashSession); sessErr == nil {
		sess.AddFlash(message, key)
		return sess.Save(r, w)
	}
	return nil
}

func (ga *GorillaAdapter) SetValue(w http.ResponseWriter, r *http.Request, key, val string, ttl int) error {
	sess, sessErr := ga.store.Get(r, ga.sessionKey)
	if sessErr != nil {
		return sessErr
	}

	sess.Values[key] = val
	sess.Options.MaxAge = ttl

	return sess.Save(r, w)
}

func (ga *GorillaAdapter) GetValue(w http.ResponseWriter, r *http.Request, key string) (string, error) {
	sess, sessErr := ga.store.Get(r, ga.sessionKey)
	if sessErr != nil {
		return "", sessErr
	}

	tok := sess.Values[key]
	return tok.(string), nil
}

func (ga *GorillaAdapter) ResetSession(w http.ResponseWriter, r *http.Request) error {
	sess, sessErr := ga.store.Get(r, ga.sessionKey)
	if sessErr != nil {
		return sessErr
	}

	sess.Options.MaxAge = -1
	return sess.Save(r, w)
}

func SetSessionKey(sessionKey string) GorillaConfig {
	return func(ga *GorillaAdapter) {
		ga.sessionKey = sessionKey
	}
}
