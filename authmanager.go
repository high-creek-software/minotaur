package minotaur

import (
	"bytes"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/high-creek-software/minotaur/magiclink"
	"gitlab.com/high-creek-software/minotaur/usertoken"

	"github.com/rs/xid"
	"gitlab.com/high-creek-software/minotaur/messaging"
	"gitlab.com/high-creek-software/minotaur/passwords"
)

const (
	defaultMagicLinkTemplate     = "magiclink"
	defaultPasswordResetTemplate = "passwordreset"
)

var ErrorPasswordResetExpired = errors.New("password reset expired")
var ErrorAuthenticatingUser = errors.New("error authenticating user")

type AuthManager[R comparable, U UserInfo[R]] interface {
	TryLogin(email, password, fingerprint string) (U, usertoken.Token, error)
	InitiateLogin(email string)
	RedeemLogin(token, tokenID, fingerprint string) (U, usertoken.Token, error)
	AuthenticateUser(userInfo U, fingerprint string) (usertoken.Token, error)
	LoadUser(token string) (U, usertoken.Token, error)
	ListUserTokens(userID string) ([]usertoken.Token, error)
	DeleteToken(token string) error
	RequestPasswordReset(email string) error
	RedeemPasswordResetRequest(passwordResetRequestID, token, password, verify string) error
}

type AuthManagerImpl[R comparable, U UserInfo[R]] struct {
	userRepo              UserInfoRepo[R, U]
	userTokenRepo         usertoken.Repo
	magicLinkRepo         magiclink.MagicLinkRepo
	passwordEncoder       passwords.PasswordEncoder
	messenger             messaging.Messenger
	sender                messaging.Sender
	magicLinkTemplate     string
	fromEmailAddress      string
	passwordResetRepo     passwords.PasswordResetRequestRepo
	passwordResetTemplate string
	passwordResetTimeout  time.Duration
}

func (a *AuthManagerImpl[R, U]) TryLogin(email, password, fingerprint string) (U, usertoken.Token, error) {
	user, err := a.userRepo.LoadUserByEmail(strings.ToLower(email))
	if err != nil {
		// TODO: Run a password encode, just to take up time
		return *new(U), usertoken.Token{}, err
	}

	// Need to check and make sure the incoming password isn't empty (because go's default string value is "")
	if password == "" {
		return *new(U), usertoken.Token{}, ErrorAuthenticatingUser
	}

	if matchErr := a.passwordEncoder.Matches(password, user.GetPassword()); matchErr != nil {
		return *new(U), usertoken.Token{}, matchErr
	}

	userTokenID := xid.New()
	userToken, tokenErr := GenerateRandomURLSafeString(64)
	if tokenErr != nil {
		return *new(U), usertoken.Token{}, tokenErr
	}
	token := usertoken.Token{
		ID:          userTokenID,
		UserID:      user.GetID(),
		Token:       userToken,
		Fingerprint: fingerprint,
	}

	tokenStoreErr := a.userTokenRepo.Store(token)
	if tokenStoreErr != nil {
		return *new(U), usertoken.Token{}, tokenStoreErr
	}

	return user, token, nil
}

func (a *AuthManagerImpl[R, U]) InitiateLogin(email string) {
	user, err := a.userRepo.LoadUserByEmail(strings.ToLower(email))
	if err != nil {
		log.Println("error loading user by email", err)
		return
	}

	token, tokenErr := GenerateRandomURLSafeString(64)
	if tokenErr != nil {
		log.Println("error generating token", tokenErr)
		return
	}

	cryptToken, cryptErr := a.passwordEncoder.Encode(token)
	if cryptErr != nil {
		log.Println("error crypting the token", cryptErr)
		return
	}

	magicLinkID := xid.New()
	magicLink := magiclink.MagicLink{ID: magicLinkID, Token: cryptToken, UserID: user.GetID()}

	storeErr := a.magicLinkRepo.Store(magicLink)
	if storeErr != nil {
		log.Println("error storing magic link", storeErr)
		return
	}

	data := map[string]interface{}{"Token": token, "ID": magicLink.ID}
	var w bytes.Buffer
	msgErr := a.messenger.RenderMessagePlain(&w, a.magicLinkTemplate, data)
	if msgErr != nil {
		log.Println("error rendering message", msgErr)
		return
	}

	sendErr := a.sender.Send(messaging.EmailImpl{Name: user.GetName(), Address: user.GetEmail()}, messaging.EmailImpl{Name: "Login", Address: a.fromEmailAddress}, messaging.MessageImpl{Plain: w.String(), Subject: "Login Requested"})
	if sendErr != nil {
		log.Println("error sending magic link message", sendErr)
	}
}

func (a *AuthManagerImpl[R, U]) RedeemLogin(token, tokenID, fingerprint string) (U, usertoken.Token, error) {
	magicLink, mlErr := a.magicLinkRepo.Find(tokenID)
	if mlErr != nil {
		log.Println("error loading magic link", mlErr)
		return *new(U), usertoken.Token{}, mlErr
	}

	compErr := a.passwordEncoder.Matches(token, magicLink.Token)
	if compErr != nil {
		log.Println("error, tokens do not match", compErr)
		return *new(U), usertoken.Token{}, compErr
	}

	userTokenID := xid.New()
	userToken, tokenErr := GenerateRandomURLSafeString(64)
	if tokenErr != nil {
		return *new(U), usertoken.Token{}, tokenErr
	}
	novoToken := usertoken.Token{
		ID:          userTokenID,
		UserID:      magicLink.UserID,
		Token:       userToken,
		Fingerprint: fingerprint,
	}

	tokenStoreErr := a.userTokenRepo.Store(novoToken)
	if tokenStoreErr != nil {
		return *new(U), usertoken.Token{}, tokenStoreErr
	}

	delErr := a.magicLinkRepo.Delete(tokenID)
	if delErr != nil {
		log.Println("error deleting magic link", delErr)
	}

	user, userErr := a.userRepo.FindUser(magicLink.UserID)
	if userErr != nil {
		log.Println("error loading user")
	}

	return user, novoToken, nil
}

func (a *AuthManagerImpl[R, U]) AuthenticateUser(userInfo U, fingerprint string) (usertoken.Token, error) {

	userTokenID := xid.New()

	userToken, tokenErr := GenerateRandomURLSafeString(64)
	if tokenErr != nil {
		return usertoken.Token{}, tokenErr
	}

	token := usertoken.Token{
		ID:          userTokenID,
		UserID:      userInfo.GetID(),
		Token:       userToken,
		Fingerprint: fingerprint,
	}

	tokenStoreErr := a.userTokenRepo.Store(token)
	if tokenStoreErr != nil {
		return usertoken.Token{}, tokenStoreErr
	}

	return token, nil
}

func (a *AuthManagerImpl[R, U]) LoadUser(token string) (U, usertoken.Token, error) {
	userToken, err := a.userTokenRepo.FindByToken(token)
	if err != nil {
		return *new(U), usertoken.Token{}, err
	}
	user, err := a.userRepo.FindUser(userToken.UserID)
	if err != nil {
		return *new(U), usertoken.Token{}, err
	}

	return user, userToken, nil
}

func (a *AuthManagerImpl[R, U]) ListUserTokens(userID string) ([]usertoken.Token, error) {
	return a.userTokenRepo.ListUserTokens(userID)
}

func (a *AuthManagerImpl[R, U]) DeleteToken(opaqueToken string) error {
	//token, err := a.userTokenRepo.FindByToken(opaqueToken)
	//if err != nil {
	//	return err
	//}
	return a.userTokenRepo.DeleteToken(opaqueToken)
}

func (a *AuthManagerImpl[R, U]) RequestPasswordReset(email string) error {
	token, err := GenerateRandomURLSafeString(50)
	if err != nil {
		log.Println("error generating reset token", err)
		return err
	}

	hashedToken, err := a.passwordEncoder.Encode(token)
	if err != nil {
		log.Println("error encrypting password reset token", err)
		return err
	}

	usr, err := a.userRepo.LoadUserByEmail(email)
	if err != nil {
		log.Println("error loading user for email in password reset", err)
		return nil
	}

	prrID := xid.New()

	prr := passwords.PasswordResetRequest{ID: prrID, Email: email, UserID: usr.GetID(), Token: hashedToken}
	strErr := a.passwordResetRepo.Store(prr)
	if strErr != nil {
		log.Println("error storing password reset request", strErr)
		return nil
	}

	data := map[string]interface{}{"ID": prrID, "Token": token}
	var plain bytes.Buffer
	err = a.messenger.RenderMessagePlain(&plain, a.passwordResetTemplate, data)
	if err != nil {
		log.Println("error executing plain template", err)
		return nil
	}
	err = a.sender.Send(messaging.EmailImpl{Name: usr.GetName(), Address: email}, messaging.EmailImpl{Name: "Password Reset", Address: a.fromEmailAddress}, messaging.MessageImpl{Plain: string(plain.Bytes())})
	if err != nil {
		log.Println("error sending password reset email", err)
	}
	return nil
}

func (a *AuthManagerImpl[R, U]) RedeemPasswordResetRequest(passwordResetRequestID, token, password, verify string) error {
	uid, err := xid.FromString(passwordResetRequestID)
	if err != nil {
		log.Println("error parsing the password reset id", err)
		return err
	}

	prr, err := a.passwordResetRepo.Find(uid)
	if err != nil {
		log.Println("error finding password reset request", err)
		return err
	}

	matchErr := a.passwordEncoder.Matches(token, prr.Token)
	if matchErr != nil {
		log.Println("tokens do not match", matchErr)
		return matchErr
	}

	if time.Now().Sub(prr.CreatedAt) > a.passwordResetTimeout {
		return ErrorPasswordResetExpired
	}

	if password != verify {
		return passwords.ErrorPasswordsMustMatch
	}

	hashedPassword, hashErr := a.passwordEncoder.Encode(password)
	if hashErr != nil {
		log.Println("error hashing passwords", hashErr)
		return hashErr
	}

	passwordErr := a.userRepo.UpdatePassword(prr.UserID, hashedPassword)

	return passwordErr
}

func (am *AuthManagerImpl[R, U]) SetupMagicLinkAuthentication(magicLinkRepo magiclink.MagicLinkRepo, messenger messaging.Messenger, sender messaging.Sender, fromEmail string) {
	am.magicLinkRepo = magicLinkRepo
	am.messenger = messenger
	am.sender = sender
	am.fromEmailAddress = fromEmail
}

func NewAuthManager[R comparable, U UserInfo[R]](userRepo UserInfoRepo[R, U], userTokenRepo usertoken.Repo, passwordEncoder passwords.PasswordEncoder, passwordResetRepo passwords.PasswordResetRequestRepo) AuthManager[R, U] {
	am := &AuthManagerImpl[R, U]{
		userRepo:              userRepo,
		userTokenRepo:         userTokenRepo,
		passwordEncoder:       passwordEncoder,
		magicLinkTemplate:     defaultMagicLinkTemplate,
		passwordResetRepo:     passwordResetRepo,
		passwordResetTemplate: defaultPasswordResetTemplate,
		passwordResetTimeout:  15 * time.Minute,
	}

	return am
}

func (am *AuthManagerImpl[R, U]) SetMessengerAndSender(messenger messaging.Messenger, sender messaging.Sender, fromAddress string) *AuthManagerImpl[R, U] {
	am.messenger = messenger
	am.sender = sender
	am.fromEmailAddress = fromAddress

	return am
}

func (am *AuthManagerImpl[R, U]) SetMagicLinkTemplateKey(templateKey string) *AuthManagerImpl[R, U] {
	am.magicLinkTemplate = templateKey

	return am
}

func (am *AuthManagerImpl[R, U]) SetPasswordResetTemplateKey(passwordResetTemplate string) *AuthManagerImpl[R, U] {
	am.passwordResetTemplate = passwordResetTemplate

	return am
}

func (am *AuthManagerImpl[R, U]) SetPasswordResetTimeout(timeout time.Duration) *AuthManagerImpl[R, U] {
	am.passwordResetTimeout = timeout

	return am
}
